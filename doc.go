/*
The address package provides a (hopefully) generic Address type for a variety of applications.

I got tired of having an address type in my shipping, ordering, and payment
packages, so I made a distinct package that works well with all those.

Normally I try to keep all of my packages self-contained, so creating this
package was a tough decision.

This package implements a basic view model struct for easy use with the
html/template package and the gorilla/scheme package.

This is my current TODO list for this package:

 * Create a portable database (SQLite/JSON) for all the StateProvinces in the various countries.
 * Implement code-based access to said DB, geared towards AJAX lookups.

Address validation is NOT planned for this package, but rather for the
jackmanlabs/shipping package, which should provide validation through USPS, UPS,
and FedEx.
*/
package address
