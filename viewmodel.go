package address

import (
	"github.com/jackmanlabs/bucket/html"
)

type ViewModel struct {
	NameFirst      string
	NameLast       string
	Company        string
	Street1        string
	Street2        string
	Street3        string
	City           string
	StateProvince  string
	StateProvinces []html.OptionString
	PostalCode     string
	Country        string
	Countries      []html.OptionString
	Phone          string
	Fax            string
	Email          string
}

type HTMLOption struct {
	Name     string
	Value    string
	Selected bool
}

func (addr *Address) ViewModel() *ViewModel {

	// This literal declaration is much prettier than explicit assignment to the
	// struct fields.

	avm := &ViewModel{
		NameFirst:      addr.NameFirst,
		NameLast:       addr.NameLast,
		Company:        addr.Company,
		Street1:        addr.Street1,
		Street2:        addr.Street2,
		Street3:        addr.Street3,
		City:           addr.City,
		StateProvince:  addr.StateProvince,
		StateProvinces: make([]html.OptionString, 0),
		PostalCode:     addr.PostalCode,
		Country:        addr.Country,
		Countries:      make([]html.OptionString, 0),
		Phone:          addr.Phone,
		Fax:            addr.Fax,
		Email:          addr.Email,
	}

	for _, state := range states {
		if state.Value == addr.StateProvince {
			state.Selected = true
		}
		avm.StateProvinces = append(avm.StateProvinces, state)
	}

	for _, country := range countries {
		if country.Value == addr.Country {
			country.Selected = true
		}
		avm.Countries = append(avm.Countries, country)
	}

	return avm
}
