package address

import ()

type Address struct {
	NameFirst     string `bson:"name_first"`
	NameLast      string `bson:"name_last"`
	Company       string
	Street1       string
	Street2       string
	Street3       string
	City          string
	StateProvince string `bson:"state_province"`
	PostalCode    string `bson:"postal_code"`
	Country       string
	Phone         string
	Fax           string
	Email         string
}
